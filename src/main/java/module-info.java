module AnimalShelterFX {
    requires javafx.controls;
    requires javafx.fxml;

    opens sample;
    exports sample;
    exports Animals;

}
