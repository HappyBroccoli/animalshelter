package Animals;

public class Cat extends Animal {

    String badHabits;

    public Cat(String name, Gender gender, String badHabits) {
        super(name, gender);
        this.badHabits = badHabits;
    }

    public String getBadHabits() {
        return badHabits;
    }

    public void setBadHabits(String badHabits) {
        this.badHabits = badHabits;
    }

    public String toString(){
        return super.toString() + " Bad habits: " + badHabits;
    }
}
