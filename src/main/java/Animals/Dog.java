package Animals;

import java.text.SimpleDateFormat;

public class Dog extends Animal{

    public long lastwalk;

    public Dog(String name, Gender gender) {
        super(name, gender);
        this.lastwalk = System.currentTimeMillis();
    }


    public boolean NeedsWalk() {
        return (System.currentTimeMillis() - lastwalk) > 0;
    }

    public String toString (){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd 'at' HH:mm:ss z");
        return  super.toString() + " Last walk was l" + formatter.format(lastwalk);
    }

}

