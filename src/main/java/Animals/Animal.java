package Animals;

import java.time.LocalDateTime;

public class Animal {
    public String name;
    public Gender gender;
    public Reservor reservedBy;

    public Animal(String name, Gender gender){
        this.name = name;
        this.gender = gender;
    }

    public boolean Reserve(String reservedBy)
    {
        if (this.reservedBy == null)
        {
            this.reservedBy = new Reservor(reservedBy, LocalDateTime.now());
            return true;
        }
        return false;
    }

    public String ToString()
    {
        String reserved = "not reserved";
        if (this.reservedBy != null)
        {
            reserved = "reserved by" + this.reservedBy.Name;
        }
        return this.name  +" " +  this.gender + " " +  reserved;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Reservor getReservedBy() {
        return reservedBy;
    }

    public void setReservedBy(Reservor reservedBy) {
        this.reservedBy = reservedBy;
    }
}
