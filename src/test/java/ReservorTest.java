import Animals.Reservor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

public class ReservorTest {
    @Test
    void TestChangedReservedName() {
        //ARRANGE
        Reservor reservor = new Reservor("Ruben", LocalDateTime.now());

        //ACT
        reservor.setName("Freek");

        //ASSERT
        String expected = "Freek";
        Assertions.assertEquals(expected, reservor.getName());
    }
}
